---
title: "Contribua"
date: 2020-05-16T06:50:53-03:00
lastmod : 2020-05-18T06:50:53-03:00
draft: false
tags:
  - guia
  - contribuir
categories:
  - todos
  - guias
  -
tudo: ["todos"]
image: "contribua.jpg"
author: "netrik182"
toc: true
---

A intenção deste site é a colaboração entre os membros da comunidade Monero no Brasil e demais países falantes da língua portuguesa. Caso queira contribuir enviando algum artigo ou tradução, é muito simples. Basta seguir os passos descritos neste post.

Todos os artigos do MoneroBrasil.org estão hospedados em um repositório no GitLab chamado [artigos-MoneroBrasil.org](https://gitlab.com/netrik182/artigos-monerobrasil.org) para acompanhamento e publicação do conteúdo. Você encontrará instruções detalhadas com fotos logo aqui no início para auxiliar aqueles que precisam de ajuda ao usar o GitLab.

Aqueles já estão acostumados a trabalhar com o git, basta submeter um "Merge Request" o novo arquivo em formato `.md` na raiz do repositório. Não se esqueça de criar um novo branch e utilizar o "Front Matter" e layout do autor fornecidos abaixo.

Ao final do guia, um exemplo de post é fornecido para fins de referência e em caso de dúvidas, por favor deixe seu comentário.

## 1. GitLab

### 1.1 Criar uma conta

> Esta etapa será necessária somente uma vez. Se você já possui uma conta, pode ir para o passo 1.2

Acesse o link do [GitLab](https://gitlab.com/users/sign_in#register-pane) e crie uma conta. Seguindo o link fornecido, você poderá acessar a página de cadastro. Registre-se para uma conta (use uma senha forte).

### 1.2 Fork do repositório

> Esta etapa será necessária somente uma vez. Se você já possui o "Fork" do repositório, pode ir para o passo 1.3

Navegue para o repositório [artigos-MoneroBrasil.org](https://gitlab.com/netrik182/artigos-monerobrasil.org), clique no botão "Fork" e selecione seu nome na tela seguinte.

{{< picture "fork.png" "fork.png" "Clique no botão Fork" >}}

Se tudo correr bem você verá uma faixa azul dizendo que seu "Fork" foi realizado com sucesso.

{{< picture "fork-success.png" "fork-success.png" "Clique no botão Fork" >}}
{{< picture "repo.jpg" "repo.jpg" "Tela do repositório" >}}

<!-- ### 1.3 Criar um novo "branch"

Para quem não está acostumado com desenvolvimento, esta etapa pode parecer não fazer sentido algum. Porém, para maior organização e controle por parte dos mantenedores, por favor seguia este passos mesmo que não entenda o motivo.

> Crie um novo branch toda vez que for submeter um post para o site.

{{< picture "branch.png" "branch.png" "Clique no botão Fork" >}}

Em seguida escolha um nome e clique em "Create branch".

{{< picture "create.png" "create.png" "Clique no botão Create branch" >}} -->

## 2. Criar um novo arquivo para o post

Na página principal, crie um novo arquivo cujo nome será o título do seu post, usando hífens **`-`** como espaço e extensão `.md`.

{{< picture "ide.png" "ide.png" "Abrir Web IDE" >}}

{{< picture "file.png" "file.png" "Criar novo arquivo" >}}

{{< picture "title.png" "title.png" "Exemplo de título" >}}

### 2.1 Configurar o "Front Matter"

A parte superior deste novo arquivo deve obrigatoriamente conter o "Front Matter", que nada mais é do que uma série de instruções que determinam o layout da página. Copie e cole exatamente como no exemplo abaixo e somente substitua as informações dos campos destacados.

```yml
---
title: "Exemplo de Título"
date: 2020-05-18
draft: true
tags:
  -
  -
categories:
  - todos
  -
image: "thumbnail.svg"
author: ""
toc: false
---
```

`title:`
  - Título do seu post entre aspas

`date:`
  - Data atual no formato AAAA-MM-DD

`author:` [opcional]
  - Seu nome de usuário, somente letras minúsculas, números e sem espaços

{{< picture "front-matter.png" "front-matter.png" "Colocar o Front Matter" >}}

Logo abaixo no Front Matter, basta inserir o texto usando a formatação Markdown. Uma dica para visualizar a formatação e como seu texto está ficando, é clicar na aba "Preview Markdown".

{{< picture "preview.png" "preview.png" "Visualizar a formatação" >}}

## 3. Finalizar o Post

Pronto, agora que você já tem tudo em mãos é só enviar seu texto para nós clicando em "Commit..." e inserir o título do seu post na mensagem do commit (Commit Message).

{{< picture "commit.png" "commit.png" "Enviar texto" >}}
{{< picture "commit2.png" "commit2.png" "Enviar2 texto" >}}

> Certifique-se que as opções "Create a new branch" e "Start a new merge request" estejam marcadas.

### 3.1 Definir o autor (opcional)

Para inserir suas informações e ser creditado como autor, copie e cole o conteúdo abaixo, e altere onde for apropriado.

```yml
- name: "netrik182"
  fullName: "netrik182"
  photo: "netrik182.png"
  url: "https://github.com/netrik182"
  bio: "It's me!"
```

`name:` [obrigatório]
  - Coloque um nome curto, somente letras minúsculas, números e sem espaços

`fullName:` [opcional]
  - Seu nome ou apelido que quer ser chamado, se não colocar nada será usado o nome fornecido acima

`photo:` [opcional]
  - Uma foto sua ou de seu "usuário" nas redes sociais (se houver)

`url:` [opcional]
  - Link para seu perfil no Twitter, LinkedIn, GitHub ou qualquer outro site

`bio:` [opcional]
  - Uma breve descrição sobre você, seus hobbies ou qualquer coisa parecida.

### 3.2 Enviar o Post para ser publicado

{{< picture "merge-request.png" "merge-request.png" "Publicar!" >}}

> Certifique-se que as opções "Delete source branch when merge request is accepted" e "Squash commits when merge request is accepted" estejam marcadas.

## 4. Exemplo arquivo

Se você seguiu corretamente todas as informações deste guia, o arquivo final ficou assim:

```txt
---
title: "Exemplo de Título"
date: 2020-05-18
draft: true
tags:
  -
  -
categories:
  - todos
  -
image: "thumbnail.svg"
author: ""
toc: false
---

# Exemplo de Título

Meu texto aqui.

Deixe uma linha em branco entre um texto e outro quando for iniciar um novo parágrafo.

## Exemplo de subtítulo

### Exemplo de sub-subtítulo

> Exemplo de citação

Lista de Tópicos
- Item 1
- Item 2
- Item 3

Lista Ordenada
1. Item 1
2. Item 2
3. Item 3

**Texto em negrito** ou somente uma **palavra**.

_Texto em itálico_ ou somente uma _palavra_.

~~Texto tracejado~~ ou somente uma ~~palavra~~.

| Coluna 1 | Coluna 2 |
|--|--|
| Item 1 | Item 2 |
| Item 3 | Item 4 |

[Meu link](https://monerobrasil.org)

![Minha imagem](https://monerobrasil.org/images/todos.jpg)

```

E que irá gerar esta página nova: [Exemplo de Título](/artigos/exemplo-de-título/)
